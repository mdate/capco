import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display table', () => {
    page.navigateTo();
    expect(page.getTable()).toBeTruthy();
  });
});
