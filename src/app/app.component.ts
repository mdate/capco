import { Component, OnInit, HostListener } from '@angular/core';
import { Customer } from './domain/customer';
import { CustomerService } from './customer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  customers: Customer[];
  cols: any[];
  tableHeight: string;
  loading: boolean;

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.loading = true;
    this.customerService.getCustomers().subscribe((response: Customer[]) => {
      this.customers = response;
      this.loading = false;
    });
    this.cols = [
      { field: 'id', header: 'ID', width: '50', align: 'right' },
      { field: 'name', header: 'Name', width: '200', align: 'left' },
      { field: 'status', header: 'Status', width: '80', align: 'center' },
      { field: 'phone', header: 'Phone', width: '200', align: 'left' },
      { field: 'email', header: 'email', width: '400', align: 'left' },
      { field: 'company', header: 'Company', width: '250', align: 'left' },
      { field: 'date_entry', header: 'Entry Date', width: '200', align: 'center' },
      { field: 'org_num', header: 'Org Number', width: '150', align: 'left' },
      { field: 'address_1', header: 'Address', width: '400', align: 'left' },
      { field: 'city', header: 'City', width: '250', align: 'left' },
      { field: 'zip', header: 'Zip', width: '100', align: 'left' },
      { field: 'geo', header: 'Geo', width: '200', align: 'left' },
      { field: 'pan', header: 'Pan', width: '200', align: 'left' },
      { field: 'pin', header: 'Pin', width: '50', align: 'left' },
      { field: 'fee', header: 'Fee', width: '100', align: 'right' },
      { field: 'guid', header: 'Guid', width: '400', align: 'center' },
      { field: 'date_exit', header: 'Exit Date', width: '200', align: 'center' },
      { field: 'date_first', header: 'First Date', width: '200', align: 'center' },
      { field: 'date_recent', header: 'Recent Date', width: '200', align: 'center' },
      { field: 'url', header: 'url', width: '250', align: 'left' }
    ];
    this.tableHeight = (window.innerHeight - 100) + 'px';
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.tableHeight = (window.innerHeight - 100) + 'px';
    const customers = this.customers; // make copy to refresh table
    this.customers = null;
    this.customers = customers;
  }

  submitCustomer(event: Event, customer: Customer) {
    this.customerService.submitCustomer(customer);
  }

}
