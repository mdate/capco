import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CustomerService } from './customer.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

let component: AppComponent;
let customerService: CustomerService;
const data = require('../assets/data/sample-data.json');

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        BrowserModule,
        TableModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
      providers: [CustomerService]
    }).compileComponents();
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    customerService = TestBed.get(CustomerService);
    spyOn(customerService, 'getCustomers').and.returnValue(Observable.of(data));
    spyOn(customerService, 'submitCustomer');
  }));
  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it(`should call service`, async(() => {
    component.ngOnInit();
    expect(component.customers).toBe(data);
  }));

  it(`should submit customer`, async(() => {
    component.submitCustomer(null, data[0]);
  }));

  it(`should resize`, async(() => {
    component.onResize(null);
  }));
});
