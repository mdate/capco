import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from './domain/customer';


@Injectable()
export class CustomerService {

    constructor(private http: HttpClient) { }

    getCustomers() {
        return this.http.get('./assets/data/sample-data.json');
    }
    submitCustomer(customer: Customer) {
        return this.http.post('/api/submit', { 'id': customer.id, 'status': customer.status }).subscribe();
    }
}
