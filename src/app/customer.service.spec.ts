import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpRequest, HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CustomerService } from './customer.service';
import { Customer } from './domain/customer';
const data = require('../assets/data/sample-data.json');

describe(`CustomerService`, () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        CustomerService
      ]
    });
  });

  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify();
  }));

  it(`should send an expected get customer request`, async(inject([CustomerService, HttpTestingController],
    (service: CustomerService, backend: HttpTestingController) => {
      service.getCustomers().subscribe();

      backend.expectOne((req: HttpRequest<any>) => {
        const body = new HttpParams({ fromString: req.body });

        return req.url === './assets/data/sample-data.json'
          && req.method === 'GET';
      }, `Get customers`);
  })));

  it(`should send an expected submit customer request`, async(inject([CustomerService, HttpTestingController],
    (service: CustomerService, backend: HttpTestingController) => {
      service.submitCustomer(data[0]);

      backend.expectOne((req: HttpRequest<any>) => {
        const body = new HttpParams({ fromString: req.body });
        return req.url === '/api/submit'
          && req.method === 'POST';
      }, `Submit customer`);
  })));

});
